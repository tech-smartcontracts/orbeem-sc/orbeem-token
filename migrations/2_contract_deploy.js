// migrations/NN_deploy_upgradeable_box.js
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const ERC721Orbeem = artifacts.require('ERC721Orbeem');

module.exports = async function (deployer) {
  const instance = await deployProxy(ERC721Orbeem, ["Orbeem", "ORB", "http://api.nft.local/asset/metadata/{id}", "", []], { deployer, initializer: '__ERC721Orbeem_init' });
  console.log('Deployed', instance.address);
};