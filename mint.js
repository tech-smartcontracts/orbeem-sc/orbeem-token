const creator = "0x4B37428f825fe94dbF6d3415D8344Fe1FF5aDD7A"
const data = {
    "@type": "ERC721",
    contract: "0x8ad551892687D30A19d4DA477472fD406A37a3EE",
    tokenId: "1",
    uri: "/ipfs/QmWLsBu6nS4ovaHbGAXprD1qEssJu4r5taQfB74sCG51tp",
    creators: [{ account: creator, value: "10000" }],
    royalties: []
}

